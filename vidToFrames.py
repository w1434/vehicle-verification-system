import os.path
from datetime import datetime
from cv2 import cv2

def take_screenshot_from_video(video):
    cap = cv2.VideoCapture(video)

    if not os.path.exists("input_dataset_from_video"):
        os.mkdir("input_dataset_from_video")
        
    if not os.path.exists("output_dataset_from_video"):
        os.mkdir("output_dataset_from_video")
        
    if not os.path.exists("old_dataset"):
        os.mkdir("old_dataset")

    while True:
        ret, frame = cap.read()
        fps = cap.get(cv2.CAP_PROP_FPS)
        sec = 1
        #print(fps)

        if ret:
            frame_id = int(round(cap.get(1)))
            #print(frame_id)
            #cv2.imshow("frame", frame)
            #cv2.waitKey(1)
            if frame_id % (round(fps) * sec) == 0:
                timestr = (datetime.now()).strftime("%m.%d.%Y_%H.%M.%S.%f")
                if video == "input.mp4":
                    cv2.imwrite(f"input_dataset_from_video/input_{timestr}.jpg", frame)
                elif video == "output.mp4":
                    cv2.imwrite(f"output_dataset_from_video/output_{timestr}.jpg", frame)
                #print(f"Take a screenshot {timestr}")

        else:
            print("[Error] Can't get the frame")
            break

    cap.release()
    cv2.destroyAllWindows()