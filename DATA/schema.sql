CREATE DATABASE IF NOT EXISTS MySQL
DEFAULT CHARACTER SET utf8;

USE MySQL;

CREATE TABLE Entry_Monitoring
(
    Id_Frame int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Government_number VARCHAR(15) NOT NULL,
    Brand VARCHAR(30) NOT NULL,
    Model VARCHAR(30),
    Release_Year VARCHAR(20),
    Time_Of_Entry DATETIME,
    INDEX (Government_number, Brand)
);

CREATE TABLE Check_Out_Monitoring
(
    Id_Frame int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Government_number VARCHAR(15) NOT NULL,
    Brand VARCHAR(30) NOT NULL,
    Model VARCHAR(30),
    Release_Year VARCHAR(20),
    Time_Check_Out DATETIME,
    INDEX (Government_number, Brand)
);

CREATE TABLE Government_number
(
    Id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Government_number VARCHAR(15),
    Brand VARCHAR(30),
    Model VARCHAR(30),
    Release_Year VARCHAR(20),
    INDEX (Government_number, Brand)
);
