import pandas as pd
from sqlalchemy import create_engine
import pymysql
import json
import urllib.request
import mysql.connector
import pyshorteners

def ApiRequest():
    # dictionary for translate
    dic = {'a': 'а', 'b': 'в', 'e': 'е', 'k': 'к', 'm': 'м', 'h': 'н', 'o': 'о', 'p': 'р', 'c': 'с', 't': 'т', 'y': 'у',
        'x': 'х'}


    def tr(x):
        t = ''
        for i in x:
            t += dic.get(i.lower(), i.lower()).upper() if i.isupper() else dic.get(i, i)
        return t


    # for entry monitoring
    # getting numbers from table
    select_query = 'SELECT * FROM Entry_Monitoring WHERE Model IS NULL OR Release_Year IS NULL'
    conn = create_engine('mysql+pymysql://root@localhost/mysql')
    AllData = pd.read_sql(select_query, conn)
    numbers = list(AllData['Government_number'])

    numbers_rus = []
    for i in range(len(numbers)):
        numbers_rus.append(numbers[i])
        numbers_rus[i] = tr(numbers_rus[i])

    # requesting info by numbers
    api_key = '6ab24b04-15a5-4778-b0e2-5d32881a541c'
    text = []
    CarModel = []
    CarYear = []
    CarMake = []
    for i in range(len(numbers_rus)):
        link = f'https://data.av100.ru/api.ashx?key={api_key}&gosnomer={numbers_rus[i]}'
        with urllib.request.urlopen(pyshorteners.Shortener().tinyurl.short(link)) as url:
            try:
                text = json.loads(url.read().decode())
                CarModel.append(text['result']['gibdd'][0]['car'])
                CarYear.append(str(text['result']['gibdd'][0]['carYear']))
                print(text['result']['gibdd'][0]['marka'])
                print(text['result']['gibdd'][0])
            except Exception:
                CarModel.append('Не найдено в базе')
                CarYear.append('Не найдено в базе')
                CarMake.append('Не найдено в базе')

    CarInfo = pd.DataFrame(list(zip(numbers, CarMake, CarModel, CarYear)),
                        columns=['Government_number', 'Brand', 'Model', 'Release_Year'])
    print(CarInfo)
    # updating car info
    update_query = '''
    UPDATE Entry_Monitoring
    SET Brand = %s, Model = %s, Release_Year = %s
    WHERE Government_number = %s;
    '''

    cnx = mysql.connector.connect(user='root', database='mysql', host='127.0.0.1')
    cursor = cnx.cursor()

    try:
        with cnx as connection:
            with connection.cursor() as cursor:
                for i in range(len(CarInfo)):
                    val_tuple = (CarInfo['Brand'].iloc[i],
                                CarInfo['Model'].iloc[i],
                                CarInfo['Release_Year'].iloc[i],
                                CarInfo['Government_number'].iloc[i],
                                )
                    for result in cursor.execute(update_query, val_tuple, multi=True):
                        if result.with_rows:
                            print(result.fetchall())
                    connection.commit()
    except Error as e:
        print(e)

    # for check_out_monitoring
    # getting numbers from table
    select_query = 'SELECT * FROM check_out_monitoring WHERE Model IS NULL OR Release_Year IS NULL'
    conn = create_engine('mysql+pymysql://root@localhost/mysql')
    AllData = pd.read_sql(select_query, conn)
    numbers = list(AllData['Government_number'])

    numbers_rus = []
    for i in range(len(numbers)):
        numbers_rus.append(numbers[i])
        numbers_rus[i] = tr(numbers_rus[i])

    # requesting info by numbers
    api_key = '6ab24b04-15a5-4778-b0e2-5d32881a541c'
    text = []
    CarModel = []
    CarYear = []
    CarMake = []
    for i in range(len(numbers_rus)):
        link = f'https://data.av100.ru/api.ashx?key={api_key}&gosnomer={numbers_rus[i]}'
        with urllib.request.urlopen(pyshorteners.Shortener().tinyurl.short(link)) as url:
            try:
                text = json.loads(url.read().decode())
                CarModel.append(text['result']['gibdd'][0]['car'])
                CarYear.append(str(text['result']['gibdd'][0]['carYear']))
                print(text['result']['gibdd'][0]['marka'])
                print(text['result']['gibdd'][0])
            except Exception:
                CarModel.append('Не найдено в базе')
                CarYear.append('Не найдено в базе')
                CarMake.append('Не найдено в базе')

    CarInfo = pd.DataFrame(list(zip(numbers, CarMake, CarModel, CarYear)),
                        columns=['Government_number', 'Brand', 'Model', 'Release_Year'])
    print(CarInfo)
    # updating car info
    update_query = '''
    UPDATE check_out_monitoring
    SET Brand = %s, Model = %s, Release_Year = %s
    WHERE Government_number = %s;
    '''

    cnx = mysql.connector.connect(user='root', database='mysql', host='127.0.0.1')
    cursor = cnx.cursor()

    try:
        with cnx as connection:
            with connection.cursor() as cursor:
                for i in range(len(CarInfo)):
                    val_tuple = (CarInfo['Brand'].iloc[i],
                                CarInfo['Model'].iloc[i],
                                CarInfo['Release_Year'].iloc[i],
                                CarInfo['Government_number'].iloc[i],
                                )
                    for result in cursor.execute(update_query, val_tuple, multi=True):
                        if result.with_rows:
                            print(result.fetchall())
                    connection.commit()
    except Error as e:
        print(e)
