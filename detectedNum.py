from tabnanny import check
import traceback
import numpy as np
import matplotlib.pyplot as plt
import cv2
import pytesseract

pytesseract.pytesseract.tesseract_cmd = 'Tesseract\\tesseract.exe'


def detected_main(main_img):
    try:
        carplate_img = cv2.imread(main_img)
        carplate_img_rgb = cv2.cvtColor(carplate_img, cv2.COLOR_BGR2RGB)
        plt.imshow(carplate_img_rgb)

        carplate_haar_cascade = cv2.CascadeClassifier("haarcascade_russian_plate_number.xml")

        def carplate_detect(image):
            carplate_overlay = image.copy()
            carplate_rects = carplate_haar_cascade.detectMultiScale(carplate_overlay, scaleFactor=1.1, minNeighbors=3)
            for x, y, w, h in carplate_rects:
                cv2.rectangle(carplate_overlay, (x, y), (x + w, y + h), (255, 0, 0), 5)
            return carplate_overlay

        def carplate_extract(image):
            carplate_rects = carplate_haar_cascade.detectMultiScale(image, scaleFactor=1.5, minNeighbors=5)
            for x, y, w, h in carplate_rects:
                carplate_img = image[y + 10:y + h - 10,
                                x + 15:x + w - 10]
                # carplate_img = image[y + 15:y + h - 10,
                #                x + 15:x + w - 20]
            return carplate_img

        def enlarge_img(image, scale_percent):
            width = int(image.shape[1] * scale_percent / 100)
            height = int(image.shape[0] * scale_percent / 100)
            dim = (width, height)
            resized_image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
            return resized_image

        carplate_extract_img = carplate_extract(carplate_img_rgb)
        carplate_extract_img = enlarge_img(carplate_extract_img, 150)


        carplate_extract_img_gray = cv2.cvtColor(carplate_extract_img, cv2.COLOR_RGB2GRAY)

        carplate_extract_img_gray_blur = cv2.medianBlur(carplate_extract_img_gray, 3)

        car_number = pytesseract.image_to_string(carplate_extract_img_gray_blur,
                                        lang='eng',
                                        config = '--psm 8'
                                        '--oem 3'
                                        '-c tessedit_char_whitelist=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
                                        ' -c tessedit_char_blacklist=.,|><:;~`/][}{-=)($%^-!')

        filter_number = ""

        for x in car_number:
            if x.isalpha():
                filter_number = "".join([filter_number, x])
            if x.isnumeric():
                filter_number = "".join([filter_number, x])

        final_number = number_verification(filter_number)

        if len(final_number) == 8 or len(final_number) == 9:
            if final_number:
                return final_number
        else:
            f = open('log.txt', "a+", encoding='utf-8')
            f.write("Неправильно распознанный номер: " + filter_number + '\n')
            f.close

    except Exception as e:
        f = open('log.txt', "a+", encoding='utf-8')
        f.write('\n' + 'С изображением ' + main_img + ' произошла ошибка:\n' + traceback.format_exc() + '\n')
        f.close

def number_verification(x):
    checked_number = []
    num = []
    cout = 0
    
    for i in x:
        if i.isalpha() and (cout == 1 or
                            cout == 2 or 
                            cout == 3 or
                            cout == 6 or
                            cout == 7 or
                            cout == 8):
            if i == 'O':
                num.append('0')
            if i == 'B':
                num.append('8')
            if i == 'A':
                num.append('4')

        elif i.isdigit() and (cout == 0 or 
                              cout == 4 or 
                              cout == 5):
            if i == '0':
                num.append('O')
            if i == '8':
                num.append('B')
            if i == '4':
                num.append('A')
        else:
            num.append(i)
        cout += 1

    for i in num:
        if i.isdigit() and (num.index(i) == 1 or
                            num.index(i) == 2 or 
                            num.index(i) == 3 or
                            num.index(i) == 6 or
                            num.index(i) == 7 or
                            num.index(i) == 8):
            checked_number.append(i)
        elif i.isalpha() and (num.index(i) == 0 or 
                              num.index(i) == 4 or 
                              num.index(i) == 5):
            checked_number.append(i)
        else:
            checked_number = []
            break
    return ''.join(checked_number)
