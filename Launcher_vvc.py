from asyncio import sleep
import time
import traceback
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from ApiRequest import ApiRequest
from main import *
from ApiRequest import *
import mysql.connector


class Handler(FileSystemEventHandler):
    def on_any_event(self, event):
        print(event.event_type, event.src_path)
    
    def on_created(self, event):
        sleep(2)
        print('start of code')
        input_num_list = []
        output_num_list = []

        files = os.listdir('watcher\\')
        for file in files:            
            if file == 'input.mp4':
                th1 = Thread(target=take_screenshot_from_video, args=["input.mp4"])
                th1.start()
            if file == 'output.mp4':
                take_screenshot_from_video('output.mp4')
    
        def check_pic(path):
            files = os.listdir(path)
            try:
                for file in files:
                    if file.startswith('input'):
                        shutil.move("input_dataset_from_video\\" + file, "old_dataset\\")
                        input_num_list.append(detected_main('old_dataset\\' + file))
                    elif file.startswith('output'):
                        shutil.move("output_dataset_from_video\\" + file, "old_dataset\\")
                        output_num_list.append(detected_main('old_dataset\\' + file))
                if os.listdir(path):
                    check_pic(path)
            except:
                print(traceback.format_exc())
        
        th2 = Thread(target=check_pic, args=['input_dataset_from_video'])
        th2.start()
        check_pic('output_dataset_from_video')   
        
        input_car_numbers = {
            'number': list(filter(None, set(input_num_list)))
        }
        
        output_car_numbers = {
            'number': list(filter(None, set(output_num_list)))
        }

        insert_query_input = '''
                INSERT INTO Entry_Monitoring
                (Government_number)
                VALUES (%s);
                '''

        insert_query_output = '''
                INSERT INTO check_out_monitoring
                (Government_number)
                VALUES (%s);
                '''

        cnx = mysql.connector.connect(user='root', database='mysql', host='127.0.0.1')

        if input_num_list:
            write_input_json(input_car_numbers)
            with cnx.cursor() as cursor:
                vals = [(number,) for number in list(input_car_numbers['number'])]
                cursor.executemany(insert_query_input, vals)
                cnx.commit()
        if output_num_list:
            write_output_json(output_car_numbers)
            with cnx.cursor() as cursor:
                vals = [(number,) for number in list(input_car_numbers['number'])]
                cursor.executemany(insert_query_output, vals)
                cnx.commit()
        
        for file in files:
            os.remove('watcher\\' + file)
        
        ApiRequest()
        
        print('end of code')

if __name__ == "__main__":
    event_handler = Handler()
    observer = Observer()
    observer.schedule(event_handler, path='watcher\\', recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
