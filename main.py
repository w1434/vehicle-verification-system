import json
from multiprocessing import Process
import os
import shutil
from threading import Thread
from time import sleep
from detectedNum import detected_main
from vidToFrames import take_screenshot_from_video
import mysql.connector


def write_input_json(x):
    try:
        data = json.load(open('input_nums.json'))
    except:
        data = []
        
    data.append(x)        
    with open('input_nums.json', 'w') as file:
        json.dump(data, file, indent=2)

def write_output_json(x):
    try:
        data = json.load(open('output_nums.json'))
    except:
        data = []
        
    data.append(x)        
    with open('output_nums.json', 'w') as file:
        json.dump(data, file, indent=2)

if __name__ == "__main__":
    input_num_list = []
    output_num_list = []

    th1 = Thread(target=take_screenshot_from_video, args=["output.mp4"])
    th1.start()
    th2 = Thread(target=take_screenshot_from_video, args=["input.mp4"])
    th2.start()
    
    def check_pic(path):
        files = os.listdir(path)
        for file in files:
            if file.startswith('input'):
                shutil.move("input_dataset_from_video\\" + file, "old_dataset\\")
                input_num_list.append(detected_main('old_dataset\\' + file))
            elif file.startswith('output'):
                shutil.move("output_dataset_from_video\\" + file, "old_dataset\\")
                output_num_list.append(detected_main('old_dataset\\' + file))
        if os.listdir(path):
            check_pic(path)
    
    sleep(1)
    th3 = Thread(target=check_pic, args=['input_dataset_from_video'])
    th3.start()
    check_pic('output_dataset_from_video')   
    
    input_car_numbers = {
        'number': list(filter(None, set(input_num_list)))
    }
    
    output_car_numbers = {
        'number': list(filter(None, set(output_num_list)))
    }
    insert_query_input = '''
    INSERT INTO Entry_Monitoring
    (Government_number)
    VALUES (%s);
    '''

    insert_query_output = '''
            INSERT INTO check_out_monitoring
            (Government_number)
            VALUES (%s);
            '''

    cnx = mysql.connector.connect(user='root', database='mysql', host='127.0.0.1')

    if input_num_list:
        write_input_json(input_car_numbers)
        with cnx.cursor() as cursor:
            vals = [(number,) for number in list(input_car_numbers['number'])]
            cursor.executemany(insert_query_input, vals)
            cnx.commit()
    if output_num_list:
        write_output_json(output_car_numbers)
        with cnx.cursor() as cursor:
            vals = [(number,) for number in list(input_car_numbers['number'])]
            cursor.executemany(insert_query_output, vals)
            cnx.commit()
